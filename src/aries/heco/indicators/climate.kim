project private namespace aries.heco.indicators.climate;
	
/* 
 * # 1. Change dynamics of forested areas: fragmentation, degradation, loss, conversion to grassland.
 */	
model im:Indicator ecology:ForestFragmentation
	observing
		ecology:ForestFragmentation named forest_fragmentation
	set to [
		def temp_ff = (nodata(forest_fragmentation) ? 1 : forest_fragmentation)
		def ind_ff = 1 - temp_ff
		return ind_ff
	]; 
	/*
	 * Similar to forest change #1 for Biodiversity.
	 */	


/*
 * # 2. Areas of soil loss, degradation and erosion.
 *    - Soil degradation caused by erosion, or soil damaged conservation status.
 */
model local:rubencc:aries.heco:im-data-global-conservation.soil_erosion_4686_30m_colombia_2011_v1
	as value of soil:Soil im:ConservationStatus;
	/* 
	 * Values: 1- Slight erosion, 2- Moderate erosion, 3- Very severe erosion, 4- Severe erosion, 5- No soil in urban areas, 
	 *         6- No soil with water bodies, 7- No soil with rocky outcrop, 8- No evidence of erosion.
	 */
	
model im:Indicator value of soil:Soil im:ConservationStatus
	observing
		value of soil:Soil im:ConservationStatus named soil_erosion_level
	lookup (soil_erosion_level, ?) into
	      soil_erosion_level	    |	soil_conservation_status  
	-------------------------------------------------------------
						8			|		0.9    ,              
						7		    |		1      ,              
						6			|		1      ,              
						5			|		1      ,              
						4			|		0.25   ,              
						3			|		0      ,              
						2			|		0.5    ,              
						1			|		0.75   ;                 						      
		 
		 
/* 
 * # 3 Burned areas.
 *    - Fire Hotspot: using global modis but enough (we can integrate https://siatac.co/puntos-de-calor/)
 */
number local:stefano.balbi:aries.heco:distance_to_burned_land_colombia_1km 
	as distance_to_burned_land; 

model im:Indicator im:Normalized distance to chemistry:Burned earth:Region
	observing
		distance_to_burned_land
	set to [
		def dbl = Math.log( 1 + distance_to_burned_land)	
		def dbl_min = 0
		def dbl_max = 13.45
		def ind_dbl = ((dbl < dbl_min) ? 0 : ((dbl > dbl_max) ? 1 : (dbl - dbl_min)/(dbl_max - dbl_min)))
		return ind_dbl
	];
	

/* 
 * # 4 Dry areas (Aridity index).
 */
model 'local:rubencc:aries.heco:im-data-global-hydrology_water_indicators_4326_colombia_2016#arid2016va'
	as ratio of im:Potential hydrology:EvapotranspiredWaterVolume to earth:PrecipitationVolume;
	/*
	 * Value:  > 0.6        High Water Deficit,
	 *         0.5 to 0.59  Water Deficit,
	 *         0.4 to 0.49  Moderate To Water Deficit,
	 *         0.3 to 0.39  Moderate,
	 *         0.2 to 0.29  Water Surplus To Moderate,
	 *         0.15 to 0.19 Water Surplus,
	 *         < 0.15       High Water Surplus
	 */ 

model im:Indicator ratio of im:Potential hydrology:EvapotranspiredWaterVolume to earth:PrecipitationVolume 	
	observing 
		ratio of im:Potential hydrology:EvapotranspiredWaterVolume to earth:PrecipitationVolume named aridity
	set to [
		def arid_min = 0.15
		def arid_max = 0.6
		def arid = ((aridity < arid_min) ? 0 : ((aridity > arid_max) ? 1 : (aridity - arid_min)/(arid_max - arid_min)))
		def ind_arid = 1 - arid
		return ind_arid
	];


/* 
 * # 5 Vegetation Carbon Storage (ARIES global model).
 */ 
@intensive(space)
number indicator_vegetation_carbon_storage
//model im:Indicator (ecology:Vegetation chemistry:Carbon im:Mass) 
	observing 
		ecology:Vegetation chemistry:Carbon im:Mass in t/ha named vegetation_carbon_mass
		set to [
			def vcs = (nodata(vegetation_carbon_mass) ? 0 : vegetation_carbon_mass)
			def vcs_min = 0
			def vcs_max = 193
			def ind_vcs = ((vcs < vcs_min) ? 0 : ((vcs > vcs_max) ? 1 : (vcs - vcs_min)/(vcs_max - vcs_min)))
			return ind_vcs 
		];  
	  	
/*
 * # Computing similarity to good condition. - CLIMATE INDEX
 * 
 * ## Climate indicators: 
 *   Note: indicator value of 0 refers to bad condition, while value of 1 refers to good condition.
 *   1- Change dynamics of forested areas: fragmentation, degradation, loss, conversion to grassland 
 *   2- Areas of soil loss, degradation and erosion 
 *   3- Burnt areas
 *   4- Dry areas (Aridity index)
 *   5- Carbon Storage (Vegetation)
 */
@documented(indicators.climate)
@parameter(name=w1, default=0.2, label="Weight", description = "Weight for Indicator Change dynamics of forested areas: fragmentation, degradation, loss, conversion to grassland.")
@parameter(name=w2, default=0.2, label="Weight", description = "Weight for Indicator Areas of soil loss, degradation and erosion.")
@parameter(name=w3, default=0.2, label="Weight", description = "Weight for Indicator Burned areas.")
@parameter(name=w4, default=0.2, label="Weight", description = "Weight for Indicator Dry areas (Aridity index).")
@parameter(name=w5, default=0.2, label="Weight", description = "Weight for Indicator Vegetation Carbon Storage.")	
model im:Indicator (value of ecology:Ecosystem for es:ClimateRegulation) named climate_dimension_indicator
	observing
		im:Indicator ecology:ForestFragmentation 															 named indicator_forest_fragmentation,
		im:Indicator value of soil:Soil im:ConservationStatus 			    								 named indicator_soil_status,
		im:Indicator im:Normalized distance to chemistry:Burned earth:Region										 named indicator_fire_hotspots,
		im:Indicator ratio of im:Potential hydrology:EvapotranspiredWaterVolume to earth:PrecipitationVolume named indicator_aridity,		
        indicator_vegetation_carbon_storage	//im:Indicator (ecology:Vegetation chemistry:Carbon im:Mass)  									 	 
	set to [
		def indicator_climate = (w1 * Math.pow(indicator_forest_fragmentation,2) + w2 * Math.pow(indicator_soil_status,2) 
						+ w3 * Math.pow(indicator_vegetation_carbon_storage,2) + w4 * Math.pow(indicator_aridity,2) + w5 * Math.pow(indicator_vegetation_carbon_storage,2))
		return Math.sqrt(indicator_climate)
	]
	, klab.data.normalize();
//	;

	



/* DISCARDED -----------------------------------------------------------------------------------------------------------------------*/

/* 
 * # 1. Change dynamics of forested areas: fragmentation, degradation, loss, conversion to grassland.
 */
//model 'local:rubencc:aries.heco:im-data-global-conservation.natural_forest_coverage_colombia_1990_v5',
//	  //'local:rubencc:aries.heco:im-data-global-conservation.natural_forest_coverage_colombia_2010_v5'
//	  'local:rubencc:aries.heco:im-data-global-conservation.natural_forest_coverage_colombia_2018_v5'
//	  as presence of im:Critical (conservation:Pristine ecology:Forest earth:Region)
//	  set to [self == 1];
//
//	  
//	   landcover:LandCoverType classified into 
//	  	landcover:Forest if 1,
//	  	//aries.heco.indicators.biodiversity:NoInformation if 3,
//	  	landcover:ArtificialSurface if in (2,5);		
//	/* 
//	 * Check the resource documentation to see what is the meaning of fort and not forest (artficial) for them.
//	 */  


/*
 * # 2. Areas of soil loss, degradation and erosion.
 *    - Soil degradation caused by erosion, or soil damaged conservation status.
 */	
//	/* 
//	 * The annotation is incorrect for sure, the existing concept were used to identify the value meanings.
//	 */  
//	classified into
//		im:Low im:Damaged if 1,
//		im:Moderate im:Damaged if 2,
//		im:VeryHigh im:Damaged if 3,
//		im:ModeratelyHigh im:Damaged if 4,  
//		landcover:Urban im:Damaged if 5, 
//		/* 
//		 * this means that the soil is urban so there is no soil to erode.
//		 */
//		landcover:WaterBody im:Damaged if 6, 
//		/* 
//		 * the same idea as the previous one.
//		 */
//		geology:Rock im:Damaged if 7, 
//		im:Minimal im:Damaged if 8;  
//	/* 
//	 * Values: 1- Slight erosion, 2- Moderate erosion, 3- Very severe erosion, 4- Severe erosion, 5- No soil in urban areas, 6- No soil with water bodies, 
//	 * 7- No soil with rocky outcrop, 8- No evidence of erosion.
//	 */


/* 
 * # 3 Burned areas.
 *    - Fire Hotspot: using global modis but enough (we can integrate https://siatac.co/puntos-de-calor/)
 */
//thing HotSpot
//	'A hot spot is defined as a thermal anomaly on the ground, which is actually a proxy for fires or potential fire spots'
//	implies earth:Location of im:Potential chemistry:Fire;
//	
//model each 'local:rubencc:aries.heco:im-data-global-conservation_burnt_areas_4326_colombia_2000_2020'
//	as HotSpot;
//	
//model presence of HotSpot //too long when running all Colombia
//	observing 
//		distance to HotSpot in m named hotspot_dist
//	set to [hotspot_dist <= 500 ? true : false];
//	
//Fix
//	each chemistry:Burned earth:Region
//	observing presence of chemistry:Burned earth:Region named burned_status
//	using gis.features.extract(select = burned_status);			  
//	


/* 
 * # 4 Dry areas (Aridity index).
 */ 
//@colormap(values = { 
//	aries.heco.indicators.climate:HighWaterDeficit: (255 80 80),
//	aries.heco.indicators.climate:WaterDeficit: (255 195 80),
//	aries.heco.indicators.climate:ModerateToWaterDeficit: (255 255 75),
//	aries.heco.indicators.climate:Moderate: (185 235 75),
//	aries.heco.indicators.climate:WaterSurplusToModerate: (75 170 0),
//	aries.heco.indicators.climate:WaterSurplus: (160 200 255),
//	aries.heco.indicators.climate:HighWaterSurplus: (75 100 160)
//})
//abstract attribute AridityCategory
//	describes ratio of im:Potential hydrology:EvapotranspiredWaterVolume to earth:PrecipitationVolume
//	has disjoint children		
//		HighWaterDeficit,
//		WaterDeficit,
//		ModerateToWaterDeficit,
//		Moderate,
//		WaterSurplusToModerate,
//		WaterSurplus,
//		HighWaterSurplus;
//
//class AridityCategoryType
//	is type of AridityCategory; 
//
//model AridityCategoryType
//	observing
//		ratio of im:Potential hydrology:EvapotranspiredWaterVolume to earth:PrecipitationVolume named aridity_index
//	lookup (aridity_index, ?) into
//	       aridity_index         |      aridity_category
//	  --------------------------------------------------------------
//		        > 0.6            |       HighWaterDeficit       ,	  
//		     0.5 to 0.59         |       WaterDeficit           ,
//		     0.4 to 0.49         |       ModerateToWaterDeficit ,
//		     0.3 to 0.39         |       Moderate               ,
//		     0.2 to 0.29         |       WaterSurplusToModerate ,
//		    0.15 to 0.19         |       WaterSurplus           ,
//		       < 0.15            |       HighWaterSurplus       ;



//define table climate_dimension as {
//	title: "Climate dimension table",
//	label: "Summary of the climate dimension indicators and the total index",
//	target: im:Indicator earth:AtmosphericBottomLayer es.nca:Condition 
//	columns: (
//		{title: "Indicator Change dynamics of forested areas: fragmentation, degradation, loss, conversion to grassland", target: im:Indicator landcover:LandCoverType, compute: avg}
//		{title: "Indicator Areas of soil loss, degradation and erosion", target: im:Indicator value of soil:Soil im:Damaged im:ConservationStatus, compute: avg }
//		{title: "Indicator Burnt areas", target: im:Indicator presence of HotSpot, compute: avg}
//		{title: "Indicator Dry areas (Aridity index)", target: im:Indicator ratio of im:Potential hydrology:EvapotranspiredWaterVolume to earth:PrecipitationVolume, compute: avg}				
//		{title: "Total Climate Index", target: im:Indicator earth:AtmosphericBottomLayer es.nca:Condition, compute: avg, style:bold}
//	),
//	rows: (
//		{title: "Value", style:bold}
//	)
//};
